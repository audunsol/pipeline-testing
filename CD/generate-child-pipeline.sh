#!/bin/bash
set -eEuo pipefail
OUTFOLDER=${1:-'CD/out'}

mkdir -p "$OUTFOLDER"


cat ./CD/child-pipeline.yml | sed 's/{COMMIT}/'"$CI_COMMIT_SHA"'/gi' | tee $OUTFOLDER/generated-child-pipeline.yml
